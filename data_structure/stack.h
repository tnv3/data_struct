/**
 * @file stack.h
 * @brief File containing prototype of functions using for stack structure
 */
#ifndef stack_H
#define stack_H
#include <stdbool.h>
void push(int data);
int pull();
/*!Store the state of stack overflow*/
bool overflowed_stack ;
/*!Store the empty state of the stack*/
bool empty_stack ;
void cout_stack();
void clear_stack();
void cout_stack_hex();
#endif

