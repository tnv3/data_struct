/**
 * @file queue.c
 * @brief File containing functions and variables using for queue structure
 */

/*!
Including header file with declared functions
*/
#include "queue.h"

#include <stddef.h>
/*!Define size of array where queue's stored*/
#define size 5

/*!Array declaration to allocate memory for storing queue*/
int que[size];

/*!Pointer to the address of the end of the array qu*/
int *end = que + size ;

/*!Pointer to the address of the first element in the queue*/
int *first = que;

/*!Pointer to the address of the current element while searching for an element*/
int *current_element = NULL ;

/*!Pointer to the address of the last element in the queue*/
int *last = NULL;

/*!Function add new element to the end of queue
* \param[in] element - value of adding element
*/
void add_queue(int element)
{
    if (last == NULL) last = que; // if adding element is first
    if (last == end ) last = que ; // if pointer last in the end of the array
    *last = element; // assign to last value of user element
     last++; // move pointer to the next array cell
}

/*!Function remove element from the end of queue
* \return removed element
*/
int removing_queue()
{
    static int remove_element;  // element will be delete from the list and removed to user
    remove_element = *first; // assign to last value of pointer first
    if (first == last -1) // if there's on;y one element in the queue
    {
        last = NULL;
        first = que;
    }
    else if (first == end )first = que; // if pointer first in the end of the array
    else first ++; // move pointer first to the next array cell
    return remove_element;
}

/*!Function display all queue's elements*/
void cout_queue()
{
    if (isempty())printf("Queue is empty!\n"); // if queue is empty print message
    else
    { current_element = first; //assign address of first to current element for iterating elements
    do // iterating elements and print its value
    {
        if (current_element == end) current_element = que;
        printf("%d\t", *current_element);
        current_element++;
    }
    while (current_element != last ); // until reaching the last element
    printf("\n");
    }
}
/*!Function clear queue*/
void clear_queue()
{
    first = que;
    last = NULL;
}
/*!Function display all queue's elements in hexadecimal format*/
void cout_queue_hex()
{
    if (isempty()) printf("Now queue is empty!\n"); // if queue is empty print message
    else
    {
     current_element = first; //assign address of first to current element for iterating elements
    do // iterating elements and print its value in hexadecimal format
    {
        if (current_element == end) current_element = que;
        printf("%d\t", *current_element);
        current_element++;
    }
    while (current_element != last); //until reaching the last element
    printf("\n");
    }
}
/*!Function check if queue is fulled
* \return true - if queue is fulled, false - if queue isn't full
*/
bool isfull()
{
    if ((first == que && last == end) | (first == last + 1))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
/*!Function check if queue is empty
* \return true - if queue is empty, false - if queue isn't empty
*/
bool isempty()
{
    if (last == NULL) return 1;
    else return 0;
}

