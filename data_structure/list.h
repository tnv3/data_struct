/**
 * @file list.h
 * @brief File containing prototype of functions using for linked list structure
 */
#ifndef list_H
#define list_H
#include "stdbool.h"
int remove_element;
void list_add(int val);
void list_push (int val);
void list_remove();
void delete_by_value(int data_list);
void delete_by_number(int number);
bool isempty_list();
void print_table();
#endif

