/**
 * @file stack.c
 * @brief File containing functions and variables using for stack structure
 */
#include "windows.h"
#include <stdbool.h>
#include "stack.h"
/*!Define size of array where stack's stored*/
#define size 5

/*!Array where queue is stored*/
static int element[size];
/*!Pointer to address of stack's bottom*/
static int *bottom = element;
/*!Pointer to address of top stack element*/
static int *top = element ;

/*!Function push new element to the stack
* \param[in] data - value of pushed element
*/
void push (int data)
{
    if (top < element + size) // if address of top element less then address end of stack
    {
        empty_stack = 1; // stack isn't empty
        *top = data; //assign value of user element to pointer top
        top++; // move top to the next array cell
    }
    if (top >= element + size) // if address of top element more then address end of stack
    {
        overflowed_stack = 1; // stack is overflowed
       return;
    }
}
/*!Function pull new element from the stack
* \return Value of pulled element
*/
 int pull ()
 {
     if (top != bottom) // if top doesn't reach bottom of stack
     {
      top--; // move top to the previous array cell
     }
     if (top == bottom) // if top  reach bottom of stack
     {
      empty_stack = 0; // stack is empty
     }
     return *top;
 }

 /*!Function display all stack's element */
void cout_stack()
{
    int* current = top - 1; // Assign address of top - 1 to iterate elements
    while (current >= bottom) // until reaching the bottom of stack print value of element
    {
        printf("%d\n",*current);
        current--;
    }
}]
 /*!Function clear stack*/
void clear_stack()
{
    overflowed_stack = 0;
    empty_stack = 0; // stack is empty
    static int *bottom = element;
    static int *top = element ;
}

 /*!Function display all stack's element in hexadecimal format */
void cout_stack_hex()
{
    if (!empty_stack) printf("Now stack is empty!\n"); // if stack is empty print message
    else
    {
    int* current = top - 1; // Assign address of top - 1 to iterate elements
    while (current >= bottom) // until reaching the bottom of stack print value of element in hex
    {
        printf("%3.04X\t",*current);
        current--;
    }
    printf("\n");
    }
}

