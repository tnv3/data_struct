/**
 * @file list.c
 * @brief File containing functions and variables using for linked list structure
 */

#include <stdio.h>
#include <stdlib.h>
#include "list.h"

/** @struct list
 *  @brief This structure describe structure of linked list
 *  @var list::val
 *  Member val contains value of stored element
 *  @var list::next
 *  Member next contains pointer to the address of the next structure in the list
 */
struct list
{
    int val;
    struct list* next;
};
/*!Pointer to address of first linked list element*/
static struct list *first = NULL;
/*!Pointer to address of first linked list element*/
static struct list *last = NULL;

/*!Function add new element to the end of the linked list
* \param[in] val - value of added element
*/
void list_add(int val)
{
    struct list* list = malloc(sizeof(struct list)); // allocate memory for new structure pointer
    list->next = NULL;
    list->val = val; // assign user value to pointer
    if (last == NULL | first == NULL) // if it's first element in the list
    {
        first = malloc(sizeof(struct list)); // allocate memory for pointer
        last = malloc(sizeof(struct list)); // allocate memory for pointer
        first = list; // new element is first
        last = list; //  new element is last
    }
    else // if itsn't first element in the list
    {
        last->next = list; // linking last element with new
        last = list; // move pointer last to the new element
    }
};
/*!Function display all linked list's elements*/
void list_cout()
{
    struct list* current = first; // pointer for iterate elements
    if (isempty_list()) printf("List is empty!:\n"); // if list is empty print message
    else {
    while (current != NULL) // until reaching last element print value
    {
        printf("%d\t",current->val);
        current = current->next;
    }
    printf("\n");
    }
};
/*!Function push new element to the linked list
* \param[in] val - value of pushed element
*/
void list_push (int val)
{
struct list *f; // pointer store value of first element
f = first;
struct list* current = malloc(sizeof(struct list)); // pointer for iterate elements
current->val  = val; // assign value of user element to current
current->next = f; // linking current element with first
first = current; // move first pointer to current
}

/*!Function delete element from the end of the linked list*/
void list_remove()
{
    struct list* current = first; // pointer for iterate elements
    remove_element = last->val;
    if (first == last) last = first = NULL; // if it was only one element
    else
    {
    while (current->next != last) current = current->next; // iterate until reach second last element
    free(current->next);  // free memory
    last = current;      // move pointer last to previous element
    }
    if (last == first) first->next = NULL; // if there are on;y 1 element
}
/*!Function check if the linked list is empty
*\return true - if linked list is empty, false - if linked list isn't empty
*/
bool isempty_list()
{
    if (first == NULL) return 1;
    else return 0;
}
/*!Function delete element with certain value from the linked list
*\param [in] data_list - value of the element to be deleted
*/
void delete_by_value (int data_list)
{
    struct list* current = first; // pointer for iterate elements
    struct list* prev = NULL; // pointer to element previous to the search
    if (first->val == data_list) // if element is first
    {
        first = first->next; // move pointer to the next element
        free(current); // free memory
    }
    else
    {
        while (current->next->val != data_list && current->next != NULL) // until  reach element previous to the search
            {
                    prev = current;
                    current = current->next;
            }
        if (current->next == NULL & current->val == data_list) // if element is last
        {
            free(last); // free memory
            last = prev; // move last to the previous element
            last->next = NULL;
        }
        else if (current->next == NULL && current->val != data_list)  printf("There are no elements with this value!\n");
        else // if element in the middle
        {
            prev -> next = current->next; // linking previous and next element
            free (current); // free memory
        }
    }
    if (last == first) first->next = NULL; // if there is only 1 element
}
/*!Function delete element by its number in the linked list
*\param [in] number - number of the element to be deleted
*/
void delete_by_number(int number)
{
    int i = 1;
    struct list* current = first; // pointer for iterate elements
    struct list* prev = NULL; // pointer to element previous to the search
    if (number == 1) // if element is first
    {
        first = first->next; // move pointer first to the next element
        free(current); // free memory
        if (first == NULL) last = NULL; // list is empty
    }
    else
    {
     while (i != number && current->next != NULL ) // until reach the number or last element
    {
            prev = current;
            current = current->next;
            i++;
    }
    if (!current->next && i != number )printf("There are no so many elements\n");
    else if (!current->next && i == number) // if element is last
    {
    free (last); // free memory
    last = prev; // move last to the previous element
    last->next = NULL;
    }
    else // if element in the middle
    {
        prev->next = current->next; // linking previous and next element
        free (current); // free memory
    }
    }
     if (last == first) first->next = NULL; // if there is only 1 element
}
/*!Function display all linked list's elements with its address and address of the pointer to the next element */
void print_table()
{
    struct list* current = first; // pointer for iterate elements
    if (isempty_list()) printf("List is empty!:\n"); // if list is empty print message
    else {
    printf("Address element:\t");
    printf("Value of element:\t");
    printf("Address of next element:\t");
    while (current != NULL) // until reach the last element print value, address and address of the pointer to the next
    {
        printf("\n");
        printf("%8p\t",current);
        printf("%8d\t",current->val);
        printf("%30p\t",current->next);
        current = current->next;
    }
    printf("\n");
    }
    if (last == first) first->next = NULL; // if there is only 1 element
}

