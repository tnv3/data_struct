/**
 * @file queue.h
 * @brief File containing prototype of functions using for queue structure
 */
#ifndef queue_H
#define queue_H
#include <stdbool.h>
void add_queue(int element);
int removing_queue();
void cout_queue();
void clear_queue();
bool isfull();
void cout_queue_hex();
bool isempty();
#endif

