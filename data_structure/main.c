/**
 * @file main.c
 * @brief File containing functions and variables to realize user menu for working with data structure
 */
/*! \mainpage Data  structures
 *
 * This project allows to create and edit data structures

 * Data structures available for operations with:
 * - \subpage stackPage "Work with stack"
 *
 * - \subpage queuePage "Work with queue"
 *
 * - \subpage listPage "Work with linked list"
 *
 * Organization the structure of the console application
 * - \ref main.c
 */
/*! \page stackPage Work with stack
 *
 * This page is about how to create and edit stack.
 * Following sections describe stack:
 * - \ref Header file (stack.h)
 * - \ref Source file (stack.c)
 */
/*! \page queuePage Work with queue
 *
 * This page is about how to create and edit queue.
 * Following sections describe queue:
 * - \ref Header file (queue.h)
 * - \ref Source file (queue.c)
 */
/*! \page listPage Work with linked list
 *
 * This page is about how to create and edit linked list.
 * Following sections describe linked list:
 * - \ref Header file (list.h)
 * - \ref Source file (list.c)
 */

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "stack.h"
#include "list.h"

/*!Store user choice in main menu*/
int choice;
/*!Store user choice in linked list menu*/
int act_list;
/*!Store user choice in stack menu*/
int act_stack;
/*!Store user choice in queue menu*/
int act_que;

/*!Function display main menu*/
void print_menu()
{
        system("cls");
        printf("Choose variant \n");
        printf("1.Create stack \n");
        printf("2.Create queue \n");
        printf("3.Create list  \n");
        printf("4.Exit  \n");
        scanf("%d", &choice);
}
/*!Function display menu to work with linked list*/
void print_menu_list()
{
            system("cls");
            printf ("Choose action:\n");
            printf ("1.Add element:\n");
            printf ("2.Push element:\n");
            printf ("3.Delete element\n");
            printf ("4.Delete element by value:\n");
            printf ("5.Display all elements of list:\n");
            printf ("6.Delete element by number:\n");
            printf ("7.Back to main menu:\n");
            printf ("8.Exit:\n");
            scanf("%d", &act_list);
}

void menu_list()
{
    static int data_list; // user element
    static int exit;
    switch (act_list)
    {
    case 1:
        printf ("Enter element\n");
        scanf ("%d", &data_list); // get user element
        list_add(data_list); // add user element
        printf("Your list now:\n");
        print_table(); // print all elements with addreses
        break;
    case 2:
        printf ("Enter element\n");
        scanf ("%d", &data_list); // get user element
        list_push(data_list); // push user element
        printf("Your list now:\n");
        print_table(); // print all elements with addreses
        break;
     case 3:
         if (isempty_list()) printf("List is empty!:\n");
         else
            {
            printf("Removed element:\n");
            list_remove(); // print removed element
            printf("%d\n",remove_element);
            printf("Your list now:\n");
            print_table(); // print all elements with addreses
            }
            break;
     case 4:
        if (isempty_list()) printf("List is empty!:\n");
        else
        {
            printf ("Enter value of element\n");
            scanf ("%d", &data_list); // get user value
            delete_by_value(data_list); // delete elements with user value
            printf("Your list now:\n");
            print_table();  // print all elements with addreses
        }
         break;
     case 5:
        print_table(); // print all elements with addreses
        break;
     case 6:
        if (isempty_list()) printf("List is empty!:\n");
        else
        {
            printf ("Enter number of element\n");
            scanf ("%d", &data_list); // get user number
            delete_by_number(data_list); // delete element with user number
            printf("Your list now:\n");
           print_table();  // print all elements with addreses
        }
    }
    printf ("Print 0 to back \n");
    scanf ("%d", &exit);
}
/*!Function display menu to work with stack*/
void print_menu_stack()
{
            system("cls");
            printf ("Choose action:\n");
            printf ("1.Push element:\n");
            printf ("2.Pop element:\n");
            printf ("3.Display all stack's elements:\n");
            printf ("4.Clear stack:\n");
            printf ("5.Back to the main menu\n");
            printf ("6.Exit\n");
            scanf ("%d", &act_stack);
}
void stack_actions()
{
    static int data_stack;
    switch (act_stack)
    {
    case 1:
                if (overflowed_stack)printf("Stack is overflowed\n"); // if stack's overflowed print message
                else
                {
                    printf ("Enter element\n");
                    scanf ("%d", &data_stack); // get user element
                    push(data_stack); // push element
                }
                printf ("Your stack now:\n"); // print all stack's element in hex
                cout_stack_hex();
                printf ("Print 0 to back \n");
                scanf ("%d", &data_stack);
                break;
     case 2:
                if (!empty_stack)printf("Stack is empty!\n"); // if stack's empty print message
                else
                {
                    printf("Popped element:\n");
                    printf("%d\n",pull()); // print popped element
                }
                printf ("Your stack now:\n");
                cout_stack_hex(); // print all stack's element in hex
                printf ("Print 0 to back \n");
                scanf ("%d", &data_stack);
                break;
     case 3:
                if (!empty_stack)printf("Stack is empty!\n"); // if stack's empty print message
                else
                 {
                    printf("Stack's elements:\n");
                    cout_stack();  // print all stack's element
                 }
                printf ("Print 0 to back \n");
                scanf ("%d", &data_stack);
                break;
     case 4:
                clear_stack(); // clear stack
                break;
    }
}
/*!Function display menu to work with queue*/
void print_menu_queue()
{
            system("cls");
            printf ("Choose action:\n");
            printf ("1.Add element:\n");
            printf ("2.Remove element:\n");
            printf ("3.Display all queue's elements:\n");
            printf ("4.Clear queue:\n");
            printf ("5.Back to main menu:\n");
            printf ("6.Exit:\n");
            scanf("%d", &act_que);
}
/*!Function realize functions to work with queue*/
void act_queue()
{
    static int data_queue; // user element
    static int exit;
    switch (act_que)
    {
    case 1:
                if (!isfull())
                {
                    printf ("Enter element\n");
                    scanf ("%d", &data_queue); // get user element
                    add_queue(data_queue); // add user element
                }
                else printf ("Queue is full:\n");
                printf("Your queue now:\n");
                cout_queue_hex(); // print all elements in hex
                printf ("Print 0 to back \n");
                scanf ("%d", &exit);
                break;
      case 2:
                if (isempty())printf("Queue is empty!\n");
                else
                {
                    printf("%d\n",removing_queue()); // remove last element
                    printf("Your queue now:\n");
                    cout_queue_hex(); // print all elements in hex
                }
                printf ("Print 0 to back \n");
                scanf ("%d", &data_queue);
                break;
      case 3:
                if (isempty())printf("Queue is empty\n!");
                else
                {
                    printf("Elements\n");
                    cout_queue(); // print all elements
                }
                printf ("Print 0 to back \n");
                scanf ("%d", &data_queue);
                break;
      case 4:
                clear_queue(); // clear queue
                break;
    }
}
int main()
{
    do
    {
    print_menu();
    switch (choice)
    {
    system("cls");
    case 1:
            do
            {
                print_menu_stack();
                stack_actions();
            }
            while (act_stack != 5);
            break;
    case 2:
        do
        {
        print_menu_queue();
        act_queue();
        }
        while (act !=5);
        break;
    case 3:
        do
         {
        print_menu_list();
        menu_list();
         }
        while (act_list != 7);
        break;
    }
    }
    while (choice !=4 && act_stack !=6 && act_list !=8 && act != 6);

}
